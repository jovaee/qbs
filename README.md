## qbs - The qBittorrent scheduler

qBittorrent lacks something very important, an actual scheduler for torrents. qbs fixes this by interfacing with the API provided and schedules yours torrents based on hourly user defined schedule.

It even comes with an optional Telegram notifier that sends a message to a Telegram chat containing all newly added torrents since the last time qbs has run.

## Setup

### Requirements

 * [qBittorrent](https://www.qbittorrent.org/)
 * [Telegram Bot](https://core.telegram.org/bots) (Optional)

### Configuration


#### Config File
Required configuration settings can be set in the `config.json` file with the following format

```json
{
    "qb_host": "localhost",
    "qb_username": "admin",
    "qb_password": "123",
    "bot_token": "tokenlol",  # Optionally for Telegram Logging
    "chat_id": "123"  # Optionally for Telegram Logging
}
```

#### Environment Variables

Or they can be specified in environment variables. Settings set in environment variables will take priority over ones in the config file.

| Type      | Name             | Required            | Description                                                     |
| :-------: | :----------------| :-----------------: | :-------------------------------------------------------------- |
| **PATH**  | /qbs/logs        | :heavy_check_mark:  | Directory where logs will be stored                             |
| **PATH**  | /qbs/config      | :heavy_check_mark:  | Directory where config and DB files are                         |
| **VAR**   | PERIOD           |                     | Inverval between scheduling runs in seconds. Defaults to 30s    |
| **VAR**   | QB_HOST          |                     | **IP:Port** combination to qBittorrent                          |
| **VAR**   | QB_USERNAME      |                     | qBittorrent login username                                      |
| **VAR**   | QB_PASSWORD      |                     | qBittorrent login password                                      |
| **VAR**   | BOT_TOKEN        |                     | Telegram bot token                                              |
| **VAR**   | CHAT_ID          |                     | Telegram chat ID to where notifications will be sent            |


## Schedule Updater

To more easily update daily download schedules `schedule_updater.py` is provided. It's a simple CLI that allows for the changing of daily hourly download states. Just follow the on-screen instructions. 

PS: It supports mouse input.
