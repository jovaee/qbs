import calendar

from clients import sqliteClient

# TODO: Find replacement, not receiving any more updates
from PyInquirer import prompt

BLOCK = "█"
CRED = '\033[91m'
CEND = '\033[0m'
BOLD = '\033[1m'
LIGHT_RED = "\033[91m"
LIGHT_GREEN = "\033[92m"

ALL_HOURS = list(range(0, 24, 1))
MAIN_MENU = {
    "type": "rawlist",
    "message": "Select a day to change download schedule for or one of the other options.",
    "name": "main_menu",
    "choices": [  # Can't use separators for better looking choices since rawlist support max 9 items
        *calendar.day_name,
        "Schedule Table",
        "Exit"
    ],
}

# Create DB connection
sqlite_client = sqliteClient(db_path="./config/qbs.db")


def generate_hour_checkboxes(day: int):
    """
    Generate the CLI schedule menu for a specific day
    """
    schedule = sqlite_client.get_daily_schedule(day)
    choices = [
        {
            "name": f"{str(hour).zfill(2)}:00 - {str(hour).zfill(2)}:59",
            "checked": active == "1"
        }
        for hour, active in enumerate(schedule)
    ]

    return {
        "type": "checkbox",
        "message": f"Changing download schedule for {calendar.day_name[day]}. Pressing Enter will save changes.",
        "name": "schedule",
        "choices": choices,
    }


def generate_schedule_table() -> list[str]:
    """
    Generate the schedule table view that looks like
        +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
        |  |00|01|02|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20|21|22|23|
        +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
        |Mo ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██
        |Tu ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██
        |We ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██
        |Th ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██
        |Fr ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██
        |Sa ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██
        |Su ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██ ██
        +--------------------------------------------------------------------------+
    The hourly schedules are colored red or green to indicate state
    """
    table = [
        "+--" * 25 + "+",
        "|  |",
    ]

    # Generate header row with hours
    for hour in ALL_HOURS:
        table[1] += f"{BOLD}{str(hour).zfill(2)}{CEND}|"

    table.append("+--" * 25 + "+")

    # Generate the data rows of the table each being a day
    for day in range(0, 7):
        schedule = sqlite_client.get_daily_schedule(day)
        day_schedule = f"|{BOLD}{calendar.day_name[day][:2]}{CEND} "
        for hour in schedule:
            color = LIGHT_RED if hour == "0" else LIGHT_GREEN
            day_schedule += f"{color}{BLOCK * 2}{CEND} "

        table.append(day_schedule)

    table.append("+--" + "---" * 24 + "+")
    return table


def parse_save_schedule(day: int, new_schedule: list[str]):
    """
    Parse the response from the CLI and update the corresponding day's schedule in the DB
    """
    active_hours = [int(ns[:2]) for ns in new_schedule]
    new_schedule_str = "".join("1" if hour in active_hours else "0" for hour in ALL_HOURS)
    sqlite_client.update_daily_schedule(day, new_schedule_str)


def main():
    while True:
        answer = list(prompt(MAIN_MENU).values())[0]
        if answer == "Exit":
            break
        elif answer == "Schedule Table":
            table = generate_schedule_table()
            for row in table:
                print(row)
        else:
            day = list(calendar.day_name).index(answer)
            choices = generate_hour_checkboxes(day)
            new_schedule = list(prompt(choices).values())[0]
            parse_save_schedule(day, new_schedule)

            # Print green ? and bold text to match rest of output
            print(f"{BOLD}{LIGHT_GREEN}? SUCCESS{CEND}", f"{BOLD}" + f"- Updated download schedule for {answer}.{CEND}")


if __name__ == "__main__":
    main()
