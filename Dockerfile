FROM python:3.13-alpine

ENV ENDLESS=true

WORKDIR /qbs

COPY requirements.txt ./
RUN pip install -U pip && pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "scheduler.py" ]
