import sqlite3


class sqliteClient:

    def __init__(self, db_path: str):
        self.conn = sqlite3.connect(db_path)

    def get_torrents(self) -> list[dict]:
        """
        Get all known entries
        """
        sql = """
        SELECT hash FROM torrents
        """
        res = self.conn.execute(sql).fetchall()

        torrents = []
        for r in res:
            torrents.append(
                {
                    'hash': r[0]
                }
            )

        return torrents

    def add_torrent(self, hash: str):
        sql = """
        INSERT INTO torrents (hash) VALUES (?)
        """
        self.conn.execute(sql, [hash])
        self.conn.commit()

    def delete_torrents(self, hashes: list[str]):
        sql = """
        DELETE FROM torrents WHERE hash IN (%s)
        """
        if hashes:
            sql = sql % ', '.join('?' * len(hashes))

            self.conn.execute(sql, hashes)
            self.conn.commit()

    def get_daily_schedule(self, day: int) -> str:
        sql = """
        SELECT hours FROM schedule WHERE day = ?
        """
        res = self.conn.execute(sql, [day]).fetchone()
        return res[0]

    def update_daily_schedule(self, day: int, schedule: str):
        sql = """
        UPDATE schedule SET hours = ? WHERE day = ?
        """
        self.conn.execute(sql, [schedule, day])
        self.conn.commit()
