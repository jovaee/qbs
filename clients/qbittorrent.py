from typing import Callable, ParamSpec, TypeVar
import requests

from exceptions.exceptions import NotLoggedInException

R = TypeVar("R")
P = ParamSpec("P")


def assert_login(f: Callable[P, R]) -> Callable[P, R]:
    """
    Checks if `logged_in` is `True` in `qBittorrentClient`. If not raises a
    `NotLoggedInException` error
    """

    def inner(self: "qBittorrentClient", *args: P.args, **kwargs: P.kwargs) -> R:
        if not self.logged_in:
            raise NotLoggedInException()

        return f(self, *args, **kwargs)

    return inner


class qBittorrentClient:
    """
    Interact with the qBittorrent API
    """

    def __init__(self, url: str):
        self.logged_in = False
        self.base_url = url

        self.headers = {'Origin': url}

    def _request(self, method, url, data=None, params=None) -> requests.Response:
        """

        :param method:
        :param url:
        :param data:
        """
        kwargs = {
            "method": method,
            "url": self.base_url + url,
            "headers": self.headers,
            "data": data,
            "params": params
        }
        if self.logged_in:
            kwargs["cookies"] = self.cookies

        return requests.request(**kwargs)

    def _set_cookie(self, sid: str):
        """

        :param sid:
        """
        self.cookies = {'SID': sid}

    def login(self, username: str, password: str) -> bool:
        """
        Login with the given credentials
        :param username:
        :param password:
        """
        result = self._request(method='POST', url='/api/v2/auth/login', data={'username': username, 'password': password})

        if result.status_code == 200:
            self.logged_in = True
            self._set_cookie(result.cookies['SID'])

            return True

        return False

    def logout(self) -> bool:
        """
        Logout from the current session
        """
        result = self._request(method='POST', url='/api/v2/auth/logout')

        if result.status_code == 200:
            self.logged_in = False
            return True

        return False

    @assert_login
    def get_torrent_list(self) -> list:
        """
        Get a list of torrents that are currently enrolled
        """
        result = self._request(method='GET', url='/api/v2/torrents/info')
        if result.status_code != 200:
            raise Exception()

        return result.json()

    @assert_login
    def stop_torrent(self, hash: str):
        """
        Stop torrent specified with the torrent's hash
        """
        self._request(method='POST', url='/api/v2/torrents/stop', data={'hashes': hash})

    @assert_login
    def stop_all_torrents(self):
        """
        Stop all the torrents that are enrolled
        """
        self._request(method='POST', url='/api/v2/torrents/stop', data={'hashes': 'all'})

    @assert_login
    def start_torrent(self, hash: str):
        """
        Start torrent specified with the torrent's hash
        """
        self._request(method='POST', url='/api/v2/torrents/start', data={'hashes': hash})

    @assert_login
    def start_all_torrents(self):
        """
        Start all the torrents that are enrolled
        """
        self._request(method='POST', url='/api/v2/torrents/start', data={'hashes': 'all'})

    @assert_login
    def get_torrent_details(self, hash: str) -> dict:
        """

        :param hash:
        """
        result = self._request(method='GET', url=f'/api/v2/torrents/properties', params={'hash': hash})
        return result.json()
