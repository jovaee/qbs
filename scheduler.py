import logging
import json
import os
import sys
import time

from dataclasses import dataclass
from datetime import datetime
from enum import Enum

from clients import qBittorrentClient, sqliteClient
from exceptions.exceptions import NotLoggedInException

CONFIG = {}  # Config for the application
CONFIG_VARIABLES = ["qb_host", "qb_username", "qb_password"]
FORMAT = '%(asctime)s - %(levelname)s - %(message)s'

# For a list of possible states that a torrent can be in check https://github.com/qbittorrent/qBittorrent/wiki/WebUI-API-(qBittorrent-4.1)#get-torrent-list
QB_DOWNLOADING = ['downloading', 'uploading', 'queuedDL', 'queuedUP', 'stalledDL', 'stalledUP', 'metaDL']
QB_STOPPED = ['pausedDL', 'pausedUP', 'stoppedDL']


class TorrentState(Enum):
    DOWNLOADING = 1
    IGNORED = 2
    STOPPED = 3
    UNKNOWN = 4


@dataclass
class StateTransitionable:
    start_state: TorrentState
    end_state: TorrentState
    is_downloading_time: bool
    reason: str = ""
    categories: list[str] | None = None
    sink: bool = False  # If True then when matched no action will be taken on this transition

    def can_transition(self, torrent: dict, is_downloading_time: bool) -> bool:
        current_state = TorrentState.DOWNLOADING if torrent['state'] in QB_DOWNLOADING else TorrentState.STOPPED if torrent['state'] in QB_STOPPED else TorrentState.UNKNOWN

        # Can this transition happen
        return current_state == self.start_state and is_downloading_time == self.is_downloading_time and (True if not self.categories else torrent['category'].lower() in (self.categories or []))


# NOTE: It's possible for multiple transitions to apply at once, the first one to match is the one that will be used
TRANSITIONS = [
    # Force stop categories
    StateTransitionable(TorrentState.DOWNLOADING, TorrentState.STOPPED, False, "Stopping torrent \"{name}\" as it was requested to always be stopped", categories=["pause", "stop"]),
    StateTransitionable(TorrentState.DOWNLOADING, TorrentState.STOPPED, True, "Stopping torrent \"{name}\" as it was requested to always be stopped", categories=["pause", "stop", ]),
    StateTransitionable(TorrentState.STOPPED, TorrentState.STOPPED, False, categories=["pause", "stop"], sink=True),
    StateTransitionable(TorrentState.STOPPED, TorrentState.STOPPED, True, categories=["pause", "stop"], sink=True),
    # Force download categories
    StateTransitionable(TorrentState.STOPPED, TorrentState.DOWNLOADING, False, "Torrent \"{name}\" was stopped, but was requested to complete download. Starting download", categories=["download"]),
    StateTransitionable(TorrentState.STOPPED, TorrentState.DOWNLOADING, True, "Torrent \"{name}\" was stopped, but was requested to complete download. Starting download", categories=["download"]),
    StateTransitionable(TorrentState.DOWNLOADING, TorrentState.DOWNLOADING, False, categories=["download"], sink=True),
    StateTransitionable(TorrentState.DOWNLOADING, TorrentState.DOWNLOADING, True, categories=["download"], sink=True),
    # Do nothing categories
    StateTransitionable(TorrentState.STOPPED, TorrentState.IGNORED, True, "Torrent \"{name}\" set to be ignored, not download", categories=["ignore"]),
    StateTransitionable(TorrentState.STOPPED, TorrentState.IGNORED, False, "Torrent \"{name}\" set to be ignored, doing nothing", categories=["ignore"]),
    StateTransitionable(TorrentState.DOWNLOADING, TorrentState.IGNORED, True, "Torrent \"{name}\" set to be ignored, doing nothing", categories=["ignore"]),
    StateTransitionable(TorrentState.DOWNLOADING, TorrentState.IGNORED, False, "Torrent \"{name}\" set to be ignored, not stopping", categories=["ignore"]),
    # Generic stop/start transitions. Should be last in list since lowest priority
    StateTransitionable(TorrentState.STOPPED, TorrentState.DOWNLOADING, True, "Downloading torrent \"{name}\""),
    StateTransitionable(TorrentState.DOWNLOADING, TorrentState.STOPPED, False, "Stopping torrent \"{name}\""),
    StateTransitionable(TorrentState.DOWNLOADING, TorrentState.DOWNLOADING, True, sink=True),
    StateTransitionable(TorrentState.STOPPED, TorrentState.STOPPED, False, sink=True),
]


if not os.path.exists('./logs'):
    os.mkdir('./logs')
if not os.path.exists('./config'):
    os.mkdir('./config')

# Setup the logger
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
handler.setFormatter(logging.Formatter(FORMAT))

logging.basicConfig(filename='./logs/log.log', filemode='a', format=FORMAT, level=logging.INFO)
logger = logging.getLogger(__name__)
logger.addHandler(handler)


def load_config_variable(config_file: dict, var: str) -> bool:
    """
    Attempt to load a variable from environment or config file. True is returned
    if the variable was found else false.
    """
    global CONFIG

    if var.upper() in os.environ:
        logger.info(f"Loading {var.upper()} from environment")
        CONFIG[var] = os.getenv(var.upper())
    elif config_file and var in config_file:
        logger.info(f"Loading {var.upper()} from config file")
        CONFIG[var] = config_file[var]
    else:
        return False

    return True


def load_config():
    """
    Load qbs configuration from environment and config file
    """
    logger.info("Loading configuration...")

    config_file = None
    if os.path.exists('./config/config.json'):
        config_file = json.loads(open('./config/config.json').read())

    missing = []
    for var in CONFIG_VARIABLES:
        found = load_config_variable(config_file, var)
        if not found:
            missing.append(var.upper())

    if missing:
        logger.error("Missing the following config variables: %s", ', '.join(missing))
        exit(1)


def main():
    """
    Main function
    """
    try:
        logger.info('Running qbs @ %s', datetime.now().isoformat())
        logger.info('Initializing connections...')

        # Create DB connection
        sqlite_client = sqliteClient(db_path="./config/qbs.db")

        # Create the client and login
        qb = qBittorrentClient(url=CONFIG['qb_host'])
        qb.login(CONFIG['qb_username'], CONFIG['qb_password'])

        hour = datetime.now().hour
        weekday = datetime.today().strftime("%A")
        logger.info('Checking scheduled state required for %s @ %s', weekday, hour)

        daily_schedule = sqlite_client.get_daily_schedule(datetime.now().weekday())
        is_downloading_time = daily_schedule[hour] == "1"

        logger.info('Current hour scheduled for %s', 'downloading' if is_downloading_time else 'sleeping')
        logger.info('Loading qBittorrent torrents...')

        # Load torrent information from qBittorrent
        all_torrents = qb.get_torrent_list()

        logger.info('Processing...')

        for torrent in all_torrents:
            for transition in TRANSITIONS:
                if transition.can_transition(torrent, is_downloading_time):
                    if not transition.sink:  # Sink transition don't have any actions
                        if transition.end_state == TorrentState.DOWNLOADING:
                            qb.start_torrent(torrent["hash"])
                        elif transition.end_state == TorrentState.STOPPED:
                            qb.stop_torrent(torrent["hash"])
                        elif transition.end_state == TorrentState.IGNORED:
                            # For now nothing has to happen here
                            pass

                        logger.info(transition.reason.format(**torrent))
                    else:
                        logger.debug("Sink transition %s for torrent %s", transition, torrent["name"])

                    # Stop after the first transition has been found
                    break
            else:
                logger.warning("No transition was found for torrent \"{name}\" in state \"{state}\"".format(**torrent))

        # And finally logout, not that it is actually necessary
        qb.logout()

        logger.info('Scheduler successfully ran')

    except NotLoggedInException:
        logger.error('You were not logged in when you were required to')
    except Exception:
        logger.exception('Something bad happened that broke qbs')


if __name__ == '__main__':
    # Load basic config settings
    period = int(os.environ.get('PERIOD', 30))
    endless = os.environ.get('ENDLESS', 'false')
    endless = endless.lower() == 'true'

    load_config()

    logger.info("Run in endless mode: %s", endless)
    if not endless:
        main()
    else:
        while True:
            main()

            time.sleep(period)
